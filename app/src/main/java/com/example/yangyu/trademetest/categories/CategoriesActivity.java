package com.example.yangyu.trademetest.categories;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.categories.adapters.CategoriesAdapter;
import com.example.yangyu.trademetest.categories.adapters.ListingsAdapter;
import com.example.yangyu.trademetest.categories.views.CategoriesFragment;
import com.example.yangyu.trademetest.categories.views.ListingsFragment;
import com.example.yangyu.trademetest.common.BaseActivity;
import com.example.yangyu.trademetest.common.BaseFragment;
import com.example.yangyu.trademetest.common.BasePresenter;
import com.example.yangyu.trademetest.common.BaseView;
import com.example.yangyu.trademetest.listingdetails.ListingDetailsActivity;
import com.example.yangyu.trademetest.models.Listing;
import com.example.yangyu.trademetest.models.Subcategory;

import butterknife.BindView;

/**
 * The categories activity.
 */

public final class CategoriesActivity extends BaseActivity
        implements BaseView, CategoriesAdapter.OnClickListener, ListingsAdapter.OnClickListener {
    @BindView(R.id.listings_placeholder)
    FrameLayout mListingsPlaceholder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (null == findFragmentByPlaceholder(R.id.categories_placeholder)) {
            addFragmentToActivity(
                    CategoriesFragment.newInstance(),
                    R.id.categories_placeholder,
                    true,
                    0,
                    0
            );
        }
    }

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return new BasePresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_categories;
    }

    @Override
    public void onItemClicked(Subcategory subcategory) {
        if (null != subcategory) {
            mListingsPlaceholder.setVisibility(View.VISIBLE);
            addFragmentToActivity(
                    ListingsFragment.newInstance(subcategory.getNumber()),
                    R.id.listings_placeholder,
                    true,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
            );
        }
    }

    @Override
    public void onItemClicked(Listing listing) {
        if (null != listing) {
            startActivity(ListingDetailsActivity.getCallingIntent(this, listing));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (null == findFragmentByPlaceholder(R.id.listings_placeholder)) {
            mListingsPlaceholder.setVisibility(View.GONE);
        }

        if (null == findFragmentByPlaceholder(R.id.categories_placeholder)) {
            finish();
        }
    }

    /**
     * Find the fragment in a particular placeholder.
     *
     * @param frameId The id of the placeholder.
     * @return The fragment contained by the placeholder.
     */
    private BaseFragment findFragmentByPlaceholder(int frameId) {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(frameId);
    }
}
