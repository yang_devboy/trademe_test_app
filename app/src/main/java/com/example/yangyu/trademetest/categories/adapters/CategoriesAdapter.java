package com.example.yangyu.trademetest.categories.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.models.Subcategory;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The adapter for the list in categories fragment.
 */

public final class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ItemViewHolder> {
    private List<Subcategory> mSubcategoryList = Collections.emptyList();

    @Nullable
    private OnClickListener mOnClickListener;

    @Inject
    public CategoriesAdapter() {

    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void replaceWith(List<Subcategory> subcategoryList) {
        mSubcategoryList = subcategoryList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mSubcategoryList.size();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final Subcategory subcategory = mSubcategoryList.get(position);
        if (null != subcategory) {
            holder.mNameTextView.setText(subcategory.getName());
            holder.mNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnClickListener != null) {
                        mOnClickListener.onItemClicked(subcategory);
                    }
                }
            });
        }
    }

    /**
     * Provide a reference to the view for the list's item.
     */
    static final class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_tv)
        TextView mNameTextView;

        ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface OnClickListener {
        void onItemClicked(Subcategory subcategory);
    }
}
