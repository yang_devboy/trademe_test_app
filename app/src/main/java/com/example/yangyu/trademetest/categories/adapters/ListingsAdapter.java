package com.example.yangyu.trademetest.categories.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.models.Listing;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The adapter for the list in listings fragment.
 */

public final class ListingsAdapter extends RecyclerView.Adapter<ListingsAdapter.ItemViewHolder> {
    private List<Listing> mListingList = Collections.emptyList();

    @Nullable
    private OnClickListener mOnClickListener;

    private final Picasso mPicasso;

    @Inject
    public ListingsAdapter(Picasso picasso) {
        mPicasso = picasso;
    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void replaceWith(List<Listing> listingList) {
        mListingList = listingList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListingList.size();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final Listing listing = mListingList.get(position);
        if (null != listing) {
            mPicasso.load(listing.getPictureUrl())
                    .fit().centerCrop()
                    .error(R.mipmap.ic_launcher)
                    .into(holder.mThumbnailImageView);
            holder.mTitleIdTextView.setText(holder.mTitleIdTextView.getContext()
                    .getString(R.string.text_listing_title_and_id, listing.getTitle(), listing.getListingId()));
            holder.mSubtitleTextView.setText(listing.getSubtitle());
            holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnClickListener != null) {
                        mOnClickListener.onItemClicked(listing);
                    }
                }
            });
        }
    }

    /**
     * Provide a reference to the view for the list's item.
     */
    static final class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.root_layout)
        RelativeLayout mRootLayout;
        @BindView(R.id.thumbnail_iv)
        ImageView mThumbnailImageView;
        @BindView(R.id.title_id_tv)
        TextView mTitleIdTextView;
        @BindView(R.id.subtitle_tv)
        TextView mSubtitleTextView;

        ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface OnClickListener {
        void onItemClicked(Listing listing);
    }
}
