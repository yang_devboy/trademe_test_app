package com.example.yangyu.trademetest.categories.di;

import com.example.yangyu.trademetest.categories.adapters.CategoriesAdapter;
import com.example.yangyu.trademetest.categories.presenters.CategoriesPresenter;
import com.example.yangyu.trademetest.categories.views.CategoriesFragment;
import com.example.yangyu.trademetest.di.components.AppComponent;
import com.example.yangyu.trademetest.di.scopes.PerFragment;

import dagger.Component;

/**
 * The component corresponding to the categories fragment.
 */

@PerFragment
@Component(dependencies = AppComponent.class)
public interface CategoriesFragmentComponent {

    void inject(CategoriesFragment categoriesFragment);

    CategoriesPresenter categoriesPresenter();

    CategoriesAdapter categoriesAdapter();

}
