package com.example.yangyu.trademetest.categories.di;

import com.example.yangyu.trademetest.categories.adapters.ListingsAdapter;
import com.example.yangyu.trademetest.categories.presenters.ListingsPresenter;
import com.example.yangyu.trademetest.categories.views.ListingsFragment;
import com.example.yangyu.trademetest.di.components.AppComponent;
import com.example.yangyu.trademetest.di.scopes.PerFragment;

import dagger.Component;

/**
 * The component corresponding to the listings fragment.
 */

@PerFragment
@Component(dependencies = AppComponent.class)
public interface ListingsFragmentComponent {

    void inject(ListingsFragment listingsFragment);

    ListingsPresenter listingsPresenter();

    ListingsAdapter listingsAdapter();

}
