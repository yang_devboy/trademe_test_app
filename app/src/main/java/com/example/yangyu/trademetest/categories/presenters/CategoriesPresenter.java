package com.example.yangyu.trademetest.categories.presenters;

import com.example.yangyu.trademetest.categories.views.CategoriesView;
import com.example.yangyu.trademetest.common.BasePresenter;
import com.example.yangyu.trademetest.di.scopes.PerFragment;
import com.example.yangyu.trademetest.models.Category;
import com.example.yangyu.trademetest.network.CatalogueService;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * The presenter for the categories fragment.
 */

@PerFragment
public final class CategoriesPresenter extends BasePresenter<CategoriesView> {
    private final CatalogueService mCatalogueService;

    private Disposable mDisposable;

    @Inject
    public CategoriesPresenter(CatalogueService catalogueService) {
        mCatalogueService = catalogueService;
    }

    /**
     * Retrieve all or part of the Trade Me category tree.
     *
     * @param number     The category number of the root of the returned tree.
     * @param fileFormat The format of the response.
     * @param depth      The depth of the category tree to return.
     */
    public void retrieveCategories(String number, String fileFormat, int depth) {
        if (isViewAttached()) {
            getView().showLoading();

            mDisposable = mCatalogueService.retrieveCategories(number, fileFormat, depth)
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Category>() {
                        @Override
                        public void onSuccess(@NonNull Category category) {
                            Logger.d("successfully obtained categories information");
                            if (isViewAttached()) {
                                getView().showCategories(category);
                                getView().hideLoading();
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Logger.e("Failed to obtain categories information");
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showError(e);
                            }
                        }
                    });
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (null != mDisposable && !mDisposable.isDisposed()) {
            Logger.d("disposable will be disposed after detaching view");
            mDisposable.dispose();
        }
    }
}
