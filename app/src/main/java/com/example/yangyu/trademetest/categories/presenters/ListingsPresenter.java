package com.example.yangyu.trademetest.categories.presenters;

import com.example.yangyu.trademetest.categories.views.ListingsView;
import com.example.yangyu.trademetest.common.BasePresenter;
import com.example.yangyu.trademetest.di.scopes.PerFragment;
import com.example.yangyu.trademetest.models.SearchResult;
import com.example.yangyu.trademetest.network.CatalogueService;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * The presenter for the listings fragment.
 */

@PerFragment
public final class ListingsPresenter extends BasePresenter<ListingsView> {
    private final CatalogueService mCatalogueService;

    private Disposable mDisposable;

    @Inject
    public ListingsPresenter(CatalogueService catalogueService) {
        mCatalogueService = catalogueService;
    }

    /**
     * Search for listings on Trade Me by category, by keywords or a combination of these two.
     *
     * @param fileFormat The format of the response.
     * @param category   Specifies the category in which you want to perform the search.
     * @param page       The page number of the set of results to return.
     * @param rows       The number of results per page.
     */
    public void searchByCategory(String fileFormat, String category, int page, int rows) {
        if (isViewAttached()) {
            getView().showLoading();

            mDisposable = mCatalogueService.searchByCategory(fileFormat, category, page, rows)
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<SearchResult>() {
                        @Override
                        public void onSuccess(@NonNull SearchResult searchResult) {
                            Logger.d("successfully obtained listings information");
                            if (isViewAttached()) {
                                getView().showListings(searchResult);
                                getView().hideLoading();
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Logger.e("Failed to obtain listings information");
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showError(e);
                            }
                        }
                    });
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (null != mDisposable && !mDisposable.isDisposed()) {
            Logger.d("disposable will be disposed after detaching view");
            mDisposable.dispose();
        }
    }
}
