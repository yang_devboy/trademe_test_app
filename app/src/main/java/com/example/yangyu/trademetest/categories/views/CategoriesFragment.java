package com.example.yangyu.trademetest.categories.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.categories.CategoriesActivity;
import com.example.yangyu.trademetest.categories.adapters.CategoriesAdapter;
import com.example.yangyu.trademetest.categories.di.CategoriesFragmentComponent;
import com.example.yangyu.trademetest.categories.di.DaggerCategoriesFragmentComponent;
import com.example.yangyu.trademetest.categories.presenters.CategoriesPresenter;
import com.example.yangyu.trademetest.common.BaseFragment;
import com.example.yangyu.trademetest.models.Category;
import com.example.yangyu.trademetest.models.Subcategory;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * The categories fragment.
 */

public final class CategoriesFragment extends BaseFragment<CategoriesView, CategoriesPresenter>
        implements CategoriesView {
    @BindView(R.id.rv)
    RecyclerView mRecyclerView;

    @Inject
    CategoriesPresenter mCategoriesPresenter;

    @Inject
    CategoriesAdapter mCategoriesAdapter;

    private ProgressDialog mProgressDialog;

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @NonNull
    @Override
    public CategoriesPresenter createPresenter() {
        return mCategoriesPresenter;
    }

    @Override
    protected void injectDependencies() {
        final CategoriesFragmentComponent categoriesFragmentComponent = DaggerCategoriesFragmentComponent.builder()
                .appComponent(getAppComponent())
                .build();
        categoriesFragmentComponent.inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_categories;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCategoriesPresenter.retrieveCategories("0", "json", 1);
    }

    @Override
    public void showLoading() {
        if (null == mProgressDialog) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(getString(R.string.message_dialog_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showCategories(Category category) {
        if (null == mRecyclerView.getAdapter()) {
            // the list has not been initialized
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mCategoriesAdapter);
            mCategoriesAdapter.setOnClickListener((CategoriesActivity) mContext);
        }

        final List<Subcategory> subcategoryList = category.getSubcategories();
        if (null != subcategoryList) {
            mCategoriesAdapter.replaceWith(subcategoryList);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
