package com.example.yangyu.trademetest.categories.views;

import com.example.yangyu.trademetest.common.BaseView;
import com.example.yangyu.trademetest.models.Category;

/**
 * The view interface for the categories fragment.
 */

public interface CategoriesView extends BaseView {

    void showLoading();

    void hideLoading();

    void showCategories(Category category);

    void showError(Throwable throwable);

}
