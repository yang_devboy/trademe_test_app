package com.example.yangyu.trademetest.categories.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.categories.CategoriesActivity;
import com.example.yangyu.trademetest.categories.adapters.ListingsAdapter;
import com.example.yangyu.trademetest.categories.di.DaggerListingsFragmentComponent;
import com.example.yangyu.trademetest.categories.di.ListingsFragmentComponent;
import com.example.yangyu.trademetest.categories.presenters.ListingsPresenter;
import com.example.yangyu.trademetest.common.BaseFragment;
import com.example.yangyu.trademetest.models.Listing;
import com.example.yangyu.trademetest.models.SearchResult;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * The listings fragment.
 */

public final class ListingsFragment extends BaseFragment<ListingsView, ListingsPresenter>
        implements ListingsView {
    @BindView(R.id.rv)
    RecyclerView mRecyclerView;

    private static final String ARG_NUMBER = "ARG_NUMBER";

    private String mNumber;

    @Inject
    ListingsPresenter mListingsPresenter;

    @Inject
    ListingsAdapter mListingsAdapter;

    private ProgressDialog mProgressDialog;

    public static ListingsFragment newInstance(String number) {
        final ListingsFragment fragment = new ListingsFragment();
        final Bundle args = new Bundle();
        args.putString(ARG_NUMBER, number);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public ListingsPresenter createPresenter() {
        return mListingsPresenter;
    }

    @Override
    protected void initArgs(Bundle args) {
        mNumber = args.getString(ARG_NUMBER);
    }

    @Override
    protected void injectDependencies() {
        final ListingsFragmentComponent listingsFragmentComponent = DaggerListingsFragmentComponent.builder()
                .appComponent(getAppComponent())
                .build();
        listingsFragmentComponent.inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_listings;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListingsPresenter.searchByCategory("json", mNumber, 1, 20);
    }

    @Override
    public void showLoading() {
        if (null == mProgressDialog) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(getString(R.string.message_dialog_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showListings(SearchResult searchResult) {
        if (null == mRecyclerView.getAdapter()) {
            // the list has not been initialized
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mListingsAdapter);
            mListingsAdapter.setOnClickListener((CategoriesActivity) mContext);
        }

        final List<Listing> listingList = searchResult.getListings();
        if (null != listingList) {
            mListingsAdapter.replaceWith(listingList);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
