package com.example.yangyu.trademetest.categories.views;

import com.example.yangyu.trademetest.common.BaseView;
import com.example.yangyu.trademetest.models.SearchResult;

/**
 * The view interface for the listings fragment.
 */

public interface ListingsView extends BaseView {

    void showLoading();

    void hideLoading();

    void showListings(SearchResult searchResult);

    void showError(Throwable throwable);

}
