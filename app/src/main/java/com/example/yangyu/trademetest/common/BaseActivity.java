package com.example.yangyu.trademetest.common;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import com.example.yangyu.trademetest.App;
import com.example.yangyu.trademetest.di.components.AppComponent;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import butterknife.ButterKnife;
import icepick.Icepick;

/**
 * Base class for all the activities. All activities are required to subclass this class.
 */

public abstract class BaseActivity<V extends BaseView, P extends BasePresenter<V>> extends MvpActivity<V, P> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        injectDependencies();

        final Bundle params = getIntent().getExtras();
        if (params != null) {
            onExtractParams(params);
        }

        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(getLayoutResId());
    }

    /**
     * Inject all the related dependencies into this activity.
     */
    protected void injectDependencies() {

    }

    /**
     * Extract the content from the bundle.
     *
     * @param params The bundle.
     */
    protected void onExtractParams(@NonNull Bundle params) {
        // default no implementation
    }

    /**
     * Return the layout resource identifier for this activity.
     *
     * @return The layout resource identifier.
     */
    protected abstract @LayoutRes int getLayoutResId();

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    /**
     * Get the app component.
     *
     * @return The app component instance.
     */
    protected AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    /**
     * Add a fragment to the container view with the corresponding id.
     *
     * @param fragment       The fragment to be added.
     * @param frameId        The container view id.
     * @param addToBackStack The add to back stack flag.
     * @param enterAnimation The entering animation.
     * @param exitAnimation  The exiting animation.
     */
    protected void addFragmentToActivity(BaseFragment fragment, int frameId,
                                         boolean addToBackStack, int enterAnimation, int exitAnimation) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(enterAnimation, exitAnimation);
        transaction.replace(frameId, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
