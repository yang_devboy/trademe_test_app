package com.example.yangyu.trademetest.common;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Defines a common interface for all the views in the app.
 */

public interface BaseView extends MvpView {
}
