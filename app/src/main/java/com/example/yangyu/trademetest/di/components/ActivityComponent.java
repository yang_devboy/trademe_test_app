package com.example.yangyu.trademetest.di.components;

import com.example.yangyu.trademetest.common.BaseActivity;
import com.example.yangyu.trademetest.di.modules.ActivityModule;
import com.example.yangyu.trademetest.di.scopes.PerActivity;

import dagger.Component;

/**
 * The component corresponding to an activity.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    BaseActivity activity();

}
