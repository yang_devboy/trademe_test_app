package com.example.yangyu.trademetest.di.components;

import com.example.yangyu.trademetest.di.modules.ApiModule;
import com.example.yangyu.trademetest.di.modules.AppModule;
import com.example.yangyu.trademetest.network.CatalogueService;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * The global application component that provides a single instance of the api network service.
 */

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    Picasso picasso();

    CatalogueService catalogueService();

}
