package com.example.yangyu.trademetest.di.modules;

import com.example.yangyu.trademetest.common.BaseActivity;
import com.example.yangyu.trademetest.di.scopes.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * The module that corresponds to an activity component.
 */

@Module
public final class ActivityModule {
    private final BaseActivity mActivity;

    public ActivityModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    BaseActivity provideActivity() {
        return mActivity;
    }
}
