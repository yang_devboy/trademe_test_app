package com.example.yangyu.trademetest.di.modules;

import com.example.yangyu.trademetest.BuildConfig;
import com.example.yangyu.trademetest.network.CatalogueService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The module that provides a single instance of retrofit and api service.
 */

@Module
public final class ApiModule {
    /**
     * Retrofit instance settings.
     */
    private static final int CONNECT_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 20;
    private static final int WRITE_TIMEOUT = 20;

    @Provides
    @Singleton
    @Named("base_url")
    String provideBaseUrl() {
        return BuildConfig.API_BASE_URL;
    }

    @Provides
    @Singleton
    @Named("consumer_key")
    String provideConsumerKey() {
        return BuildConfig.API_CONSUMER_KEY;
    }

    @Provides
    @Singleton
    @Named("consumer_secret")
    String provideConsumerSecret() {
        return BuildConfig.API_CONSUMER_SECRET;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    Interceptor provideOAuth1Interceptor(@Named("consumer_key") final String consumerKey,
                                         @Named("consumer_secret") final String consumerSecret) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();

                final String authorization = "OAuth oauth_consumer_key=\""
                        + consumerKey
                        + "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\""
                        + consumerSecret + "&\"";

                final Request request = original.newBuilder()
                        .addHeader("Authorization", authorization)
                        .build();

                return chain.proceed(request);
            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor logging, Interceptor oauth1Interceptor) {
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // show logs for debug mode.
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(logging);
        }
        // add oauth1 signing interceptor.
        httpClient.addInterceptor(oauth1Interceptor);
        // set different time out.
        httpClient.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        httpClient.retryOnConnectionFailure(true);

        return httpClient.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(@Named("base_url") String baseUrl, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    CatalogueService provideCatalogueService(Retrofit retrofit) {
        return retrofit.create(CatalogueService.class);
    }
}
