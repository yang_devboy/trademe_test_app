package com.example.yangyu.trademetest.di.modules;

import android.app.Application;
import android.net.Uri;

import com.example.yangyu.trademetest.App;
import com.example.yangyu.trademetest.di.qualifiers.ForApplication;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The module that corresponds to the application component.
 */

@Module
public final class AppModule {
    private final App mApp;

    public AppModule(final App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @ForApplication
    Application provideApplication() {
        return mApp;
    }

    @Provides
    @Singleton
    Picasso providePicasso(@ForApplication Application app) {
        return new Picasso.Builder(app).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                Logger.e(exception, "Failed to load image: %s", uri);
            }
        }).build();
    }
}
