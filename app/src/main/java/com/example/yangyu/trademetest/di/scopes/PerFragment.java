package com.example.yangyu.trademetest.di.scopes;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The scope that corresponds to the life cycle of a fragment.
 */

@Scope
@Documented
@Retention(RUNTIME)
public @interface PerFragment {
}
