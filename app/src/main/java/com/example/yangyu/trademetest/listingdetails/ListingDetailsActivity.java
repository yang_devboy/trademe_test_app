package com.example.yangyu.trademetest.listingdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yangyu.trademetest.R;
import com.example.yangyu.trademetest.common.BaseActivity;
import com.example.yangyu.trademetest.common.BasePresenter;
import com.example.yangyu.trademetest.models.Listing;

import butterknife.BindView;
import icepick.State;

/**
 * The listing details activity.
 */

public final class ListingDetailsActivity extends BaseActivity {
    @BindView(R.id.picture_iv)
    ImageView mPictureImageView;
    @BindView(R.id.title_id_tv)
    TextView mTitleIdTextView;
    @BindView(R.id.subtitle_tv)
    TextView mSubtitleTextView;

    private static final String EXTRA_LISTING_PICTURE_URL = "EXTRA_LISTING_PICTURE_URL";
    private static final String EXTRA_LISTING_ID = "EXTRA_LISTING_ID";
    private static final String EXTRA_LISTING_TITLE = "EXTRA_LISTING_TITLE";
    private static final String EXTRA_LISTING_SUBTITLE = "EXTRA_LISTING_SUBTITLE";

    @State
    String mPictureUrl;
    @State
    long mListingId;
    @State
    String mListingTitle;
    @State
    String mListingSubtitle;

    /**
     * Return an intent to start this activity.
     *
     * @param context The context to start this activity.
     * @param listing The listing passed to this activity.
     * @return An intent to start this activity.
     */
    public static Intent getCallingIntent(Context context, Listing listing) {
        final Intent intent = new Intent(context, ListingDetailsActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_LISTING_PICTURE_URL, listing.getPictureUrl());
        bundle.putLong(EXTRA_LISTING_ID, listing.getListingId());
        bundle.putString(EXTRA_LISTING_TITLE, listing.getTitle());
        bundle.putString(EXTRA_LISTING_SUBTITLE, listing.getSubtitle());
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getAppComponent().picasso()
                .load(mPictureUrl)
                .fit()
                .centerCrop()
                .error(R.mipmap.ic_launcher)
                .into(mPictureImageView);
        mTitleIdTextView.setText(getString(R.string.text_listing_title_and_id, mListingTitle, mListingId));
        mSubtitleTextView.setText(mListingSubtitle);
    }

    @Override
    protected void onExtractParams(@NonNull Bundle params) {
        mPictureUrl = params.getString(EXTRA_LISTING_PICTURE_URL);
        mListingId = params.getLong(EXTRA_LISTING_ID);
        mListingTitle = params.getString(EXTRA_LISTING_TITLE);
        mListingSubtitle = params.getString(EXTRA_LISTING_SUBTITLE);
    }

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return new BasePresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_listing_details;
    }
}
