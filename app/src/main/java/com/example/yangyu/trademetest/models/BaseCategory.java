package com.example.yangyu.trademetest.models;

import com.google.gson.annotations.SerializedName;

/**
 * The common attributes shared by all related categories model.
 */

public class BaseCategory {
    @SerializedName("Name")
    private String mName;
    @SerializedName("Number")
    private String mNumber;
    @SerializedName("Path")
    private String mPath;
    @SerializedName("IsLeaf")
    private boolean mIsLeaf;

    public String getName() {
        return mName;
    }

    public String getNumber() {
        return mNumber;
    }

    public String getPath() {
        return mPath;
    }

    public boolean isLeaf() {
        return mIsLeaf;
    }
}
