package com.example.yangyu.trademetest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The root category of the Trade Me category tree.
 */

public final class Category extends BaseCategory {
    @SerializedName("Subcategories")
    private List<Subcategory> mSubcategories;

    public List<Subcategory> getSubcategories() {
        return mSubcategories;
    }
}
