package com.example.yangyu.trademetest.models;

import com.google.gson.annotations.SerializedName;

/**
 * A single listing in the search result.
 */

public final class Listing {
    @SerializedName("ListingId")
    private long mListingId;
    @SerializedName("Title")
    private String mTitle;
    @SerializedName("Subtitle")
    private String mSubtitle;
    @SerializedName("PictureHref")
    private String mPictureUrl;

    public long getListingId() {
        return mListingId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getSubtitle() {
        return (null == mSubtitle ? "" : mSubtitle);
    }

    public String getPictureUrl() {
        return mPictureUrl;
    }
}
