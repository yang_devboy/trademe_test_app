package com.example.yangyu.trademetest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The search result when searching for listings on Trade Me.
 */

public final class SearchResult {
    @SerializedName("TotalCount")
    private int mTotalCount;
    @SerializedName("Page")
    private int mPage;
    @SerializedName("PageSize")
    private int mPageSize;
    @SerializedName("List")
    private List<Listing> mListings;

    public int getTotalCount() {
        return mTotalCount;
    }

    public int getPage() {
        return mPage;
    }

    public int getPageSize() {
        return mPageSize;
    }

    public List<Listing> getListings() {
        return mListings;
    }
}
