package com.example.yangyu.trademetest.models;

import com.google.gson.annotations.SerializedName;

/**
 * The subcategory contained by its parent category.
 */

public final class Subcategory extends BaseCategory {
    @SerializedName("HasClassifieds")
    private boolean mHasClassifieds;
    @SerializedName("CanHaveSecondCategory")
    private boolean mCanHaveSecondCategory;
    @SerializedName("CanBeSecondCategory")
    private boolean mCanBeSecondCategory;

    public boolean hasClassifieds() {
        return mHasClassifieds;
    }

    public boolean canHaveSecondCategory() {
        return mCanHaveSecondCategory;
    }

    public boolean canBeSecondCategory() {
        return mCanBeSecondCategory;
    }
}
