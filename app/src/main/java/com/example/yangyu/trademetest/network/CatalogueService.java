package com.example.yangyu.trademetest.network;

import com.example.yangyu.trademetest.models.Category;
import com.example.yangyu.trademetest.models.SearchResult;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * This class defines the catalogue related api endpoints used in the app.
 */

public interface CatalogueService {

    /**
     * Retrieve all or part of the Trade Me category tree.
     *
     * @param number     The category number of the root of the returned tree.
     * @param fileFormat The format of the response.
     * @param depth      The depth of the category tree to return.
     * @return A category tree containing all or part of the general categories.
     */
    @GET("Categories/{number}.{file_format}")
    Single<Category> retrieveCategories(@Path("number") String number,
                                        @Path("file_format") String fileFormat,
                                        @Query("depth") int depth);

    /**
     * Search for listings on Trade Me by category, by keywords or a combination of these two.
     *
     * @param fileFormat The format of the response.
     * @param category   Specifies the category in which you want to perform the search.
     * @param page       The page number of the set of results to return.
     * @param rows       The number of results per page.
     * @return The search result containing a list of listings.
     */
    @GET("Search/General.{file_format}")
    Single<SearchResult> searchByCategory(@Path("file_format") String fileFormat,
                                          @Query("category") String category,
                                          @Query("page") int page,
                                          @Query("rows") int rows);

}
