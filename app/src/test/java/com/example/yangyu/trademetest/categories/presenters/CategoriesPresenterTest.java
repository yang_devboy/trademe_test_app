package com.example.yangyu.trademetest.categories.presenters;

import com.example.yangyu.trademetest.categories.views.CategoriesView;
import com.example.yangyu.trademetest.models.Category;
import com.example.yangyu.trademetest.network.CatalogueService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * A bunch of unit tests for the categories presenter.
 */

@RunWith(MockitoJUnitRunner.class)
public class CategoriesPresenterTest {
    @Rule
    public final RxSchedulersOverrideRule schedulers = new RxSchedulersOverrideRule();

    private CategoriesPresenter mCategoriesPresenter;

    @Mock
    private CategoriesView mCategoriesView;

    @Mock
    private CatalogueService mCatalogueService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mCategoriesPresenter = new CategoriesPresenter(mCatalogueService);
    }

    @Test
    public void test_retrieveCategories_success() throws Exception {
        mCategoriesPresenter.attachView(mCategoriesView);
        assertTrue(mCategoriesPresenter.isViewAttached());

        when(mCatalogueService.retrieveCategories(anyString(), anyString(), anyInt()))
                .thenReturn(Single.just(new Category()));
        mCategoriesPresenter.retrieveCategories("0", "json", 1);

        verify(mCategoriesView).showLoading();
        verify(mCategoriesView).showCategories(any(Category.class));
        verify(mCategoriesView).hideLoading();
    }

    @Test
    public void test_retrieveCategories_fail() throws Exception {
        mCategoriesPresenter.attachView(mCategoriesView);
        assertTrue(mCategoriesPresenter.isViewAttached());

        when(mCatalogueService.retrieveCategories(anyString(), anyString(), anyInt()))
                .thenReturn(Single.<Category>error(new RuntimeException()));
        mCategoriesPresenter.retrieveCategories("0", "json", 1);

        verify(mCategoriesView).showLoading();
        verify(mCategoriesView).hideLoading();
        verify(mCategoriesView).showError(any(Throwable.class));
    }

    @Test
    public void test_detachView() throws Exception {
        mCategoriesPresenter.attachView(mCategoriesView);
        assertTrue(mCategoriesPresenter.isViewAttached());

        mCategoriesPresenter.detachView(true);
        assertFalse(mCategoriesPresenter.isViewAttached());
    }
}