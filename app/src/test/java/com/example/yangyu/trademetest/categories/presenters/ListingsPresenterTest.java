package com.example.yangyu.trademetest.categories.presenters;

import com.example.yangyu.trademetest.categories.views.ListingsView;
import com.example.yangyu.trademetest.models.SearchResult;
import com.example.yangyu.trademetest.network.CatalogueService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * A bunch of unit tests for the listings presenter.
 */

@RunWith(MockitoJUnitRunner.class)
public class ListingsPresenterTest {
    @Rule
    public final RxSchedulersOverrideRule schedulers = new RxSchedulersOverrideRule();

    private ListingsPresenter mListingsPresenter;

    @Mock
    private ListingsView mListingsView;

    @Mock
    private CatalogueService mCatalogueService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mListingsPresenter = new ListingsPresenter(mCatalogueService);
    }

    @Test
    public void test_searchByCategory_success() throws Exception {
        mListingsPresenter.attachView(mListingsView);
        assertTrue(mListingsPresenter.isViewAttached());

        when(mCatalogueService.searchByCategory(anyString(), anyString(), anyInt(), anyInt()))
                .thenReturn(Single.just(new SearchResult()));
        mListingsPresenter.searchByCategory("json", "0", 1, 20);

        verify(mListingsView).showLoading();
        verify(mListingsView).showListings(any(SearchResult.class));
        verify(mListingsView).hideLoading();
    }

    @Test
    public void test_searchByCategory_fail() throws Exception {
        mListingsPresenter.attachView(mListingsView);
        assertTrue(mListingsPresenter.isViewAttached());

        when(mCatalogueService.searchByCategory(anyString(), anyString(), anyInt(), anyInt()))
                .thenReturn(Single.<SearchResult>error(new RuntimeException()));
        mListingsPresenter.searchByCategory("json", "0", 1, 20);

        verify(mListingsView).showLoading();
        verify(mListingsView).hideLoading();
        verify(mListingsView).showError(any(Throwable.class));
    }

    @Test
    public void test_detachView() throws Exception {
        mListingsPresenter.attachView(mListingsView);
        assertTrue(mListingsPresenter.isViewAttached());

        mListingsPresenter.detachView(true);
        assertFalse(mListingsPresenter.isViewAttached());
    }
}